///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01 - Hello World
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date  12 Jan 2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");
}

